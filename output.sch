EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Simple Speaker/Headphone Output Attenuator"
Date ""
Rev ""
Comp "Lyle Hanson"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Speaker LS1
U 1 1 61816EF4
P 6200 3850
F 0 "LS1" H 6370 3846 50  0000 L CNN
F 1 "Speaker" H 6370 3755 50  0000 L CNN
F 2 "" H 6200 3650 50  0001 C CNN
F 3 "~" H 6190 3800 50  0001 C CNN
	1    6200 3850
	1    0    0    -1  
$EndComp
$Comp
L Connector:AudioJack2_SwitchT J2
U 1 1 6182FA43
P 4800 4800
F 0 "J2" H 4832 5125 50  0000 C CNN
F 1 "Headphones" H 4832 5034 50  0000 C CNN
F 2 "" H 4800 4800 50  0001 C CNN
F 3 "~" H 4800 4800 50  0001 C CNN
	1    4800 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV1
U 1 1 61832667
P 5550 3150
F 0 "RV1" V 5343 3150 50  0000 C CNN
F 1 "10K" V 5434 3150 50  0000 C CNN
F 2 "" H 5550 3150 50  0001 C CNN
F 3 "~" H 5550 3150 50  0001 C CNN
	1    5550 3150
	0    -1   1    0   
$EndComp
Text Notes 5400 2850 0    50   ~ 0
Volume
Wire Wire Line
	5000 3850 5400 3850
Connection ~ 5400 3850
Wire Wire Line
	5400 3850 6000 3850
Wire Wire Line
	5000 4700 5400 4700
Wire Wire Line
	5400 4700 5400 3850
$Comp
L Connector:AudioJack2_SwitchT J1
U 1 1 61831449
P 4800 3950
F 0 "J1" H 4832 4275 50  0000 C CNN
F 1 "Audio Input" H 4832 4184 50  0000 C CNN
F 2 "" H 4800 3950 50  0001 C CNN
F 3 "~" H 4800 3950 50  0001 C CNN
	1    4800 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 3150 5400 3850
Wire Wire Line
	5550 4800 5000 4800
Wire Wire Line
	5000 4900 6000 4900
Wire Wire Line
	6000 4900 6000 3950
$Comp
L Device:R R1
U 1 1 61CA4411
P 5550 3550
F 0 "R1" H 5620 3596 50  0000 L CNN
F 1 "100" H 5620 3505 50  0000 L CNN
F 2 "" V 5480 3550 50  0001 C CNN
F 3 "~" H 5550 3550 50  0001 C CNN
	1    5550 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3300 5550 3400
Wire Wire Line
	5550 3700 5550 4800
Wire Wire Line
	5700 3150 5800 3150
Wire Wire Line
	5800 3150 5800 3950
Wire Wire Line
	5000 3950 5800 3950
$EndSCHEMATC

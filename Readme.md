# Passive Output

This is a simple module which allows you to patch input to a small speaker or
a headphone jack and attenuate the volume. When the headphones are plugged in,
the speaker is bypassed.

If buffering or gain are required, this is not the thing.

## TODO:

- [ ] Multi-channel mono input, stereo output
